#include <LiquidCrystal.h>
#include <Encoder.h>
#include <Bounce2.h>
#include <Keyboard.h>


const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2, b1 = 13, b2 = 10, b3 = 9, b4 = 8;
const int latchPin = 1, clockPin = 21, dataPin = 0;
const int thousands = 20, hundreds = 19, tens = 18, ones = 17;
elapsedMillis keyTimer;
elapsedMillis pressTimer;
short isKeyPressed = 0;
int digitEnumerator = 1;
unsigned int iterationCounter = 0;
byte data;
byte dataArray[12];

const int stIdle = 0, stRunning = 1, stSetKey = 2, stSetIterations = 3, stSetLoopTime = 4;
int state;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
Encoder myEnc(6, 7);

Bounce debouncer1 = Bounce();
Bounce debouncer2 = Bounce();
Bounce debouncer3 = Bounce();
Bounce debouncer4 = Bounce();

unsigned int oldPosition  = 0;

char key = 32; //space
unsigned int iterations = 801;
unsigned int loopTime = 283;
unsigned int loopTime2 = 200;
unsigned int pressTime = 25;

void setup() {
  // put your setup code here, to run once:
  // set state to idle
  state = 0;
  
  // setup pins for shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  // setup pins for seven segment display common anodes
  pinMode(thousands, OUTPUT);
  pinMode(hundreds, OUTPUT);
  pinMode(tens, OUTPUT);
  pinMode(ones, OUTPUT);

  // set arrays to output index number to seven segment (10 for empty, 11 for dot)
  dataArray[0] = 0x7E; //0b01111110
  dataArray[1] = 0x0C; //0b00001100
  dataArray[2] = 0xB6; //0b10110110
  dataArray[3] = 0x9E; //0b10011110
  dataArray[4] = 0xCC; //0b11001100
  dataArray[5] = 0xDA; //0b11011010
  dataArray[6] = 0xFA; //0b11111010
  dataArray[7] = 0x0E; //0b00001110
  dataArray[8] = 0xFE; //0b11111110
  dataArray[9] = 0xDE; //0b11011110
  dataArray[10] = 0x00; //0b00000000
  dataArray[11] = 0x01; //0b00000001

  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  
  pinMode(b1,INPUT_PULLUP);
  debouncer1.attach(b1);
  debouncer1.interval(5);
  pinMode(b2,INPUT_PULLUP);
  debouncer2.attach(b2);
  debouncer2.interval(5);
  pinMode(b3,INPUT_PULLUP);
  debouncer3.attach(b3);
  debouncer3.interval(5);
  pinMode(b4,INPUT_PULLUP);
  debouncer4.attach(b4);
  debouncer4.interval(5);
  // Print a message to the LCD.
  lcd.setCursor(0,0);
  lcd.print("HLD Cheater         ");
  lcd.setCursor(0,2);
  lcd.print("run:");
  lcd.setCursor(0,3);
  lcd.print("strt");
  lcd.setCursor(5,2);
  lcd.print("#ms2:");
  lcd.setCursor(5,3);
  lcd.print(loopTime2);
  lcd.setCursor(10,2);
  lcd.print("num:");
  lcd.setCursor(10,3);
  lcd.print(iterations);
  lcd.setCursor(15,2);
  lcd.print("#ms:");
  lcd.setCursor(15,3);
  lcd.print(loopTime);

  // initialise seven segment display with only dot on first digit
  digitalWrite(thousands, LOW);
  digitalWrite(hundreds, HIGH);
  digitalWrite(tens, HIGH);
  digitalWrite(ones, HIGH);
  data = dataArray[11];
  digitalWrite(latchPin, 0);
  shiftOut(dataPin, clockPin, data);
  digitalWrite(latchPin, 1);
}

void loop() { 
  debouncer1.update();
  debouncer2.update();
  debouncer3.update();
  debouncer4.update();
  // start/stop switch
  if (debouncer1.fallingEdge()) {
    if(state == stIdle) {
      state = stRunning;
      lcd.setCursor(0,3);
      lcd.print("stop");
    } else if(state == stRunning) {
      state = stIdle;
      iterationCounter = 0;
      lcd.setCursor(0,3);
      lcd.print("strt");
    }
  }
  // set which key should be pressed by ASCII code
  //if (debouncer2.fallingEdge()) {
  //  if(state == stIdle) {
  //    state = stSetKey;
  //    myEnc.write(int(key));
  //    lcd.setCursor(0,0);
  //    lcd.print("Set key to press:   ");
  //    lcd.blink();
  //  } else if(state == stSetKey) {
  //    state = stIdle;
  //    lcd.noBlink();
  //    lcd.setCursor(0,0);
  //    lcd.print("HLD Cheater         ");
  //    lcd.setCursor(0,1);
  //    lcd.print("                    ");
  //    lcd.setCursor(5,3);
  //    if (key == 32) { // space would be invisible
  //      lcd.print("[ ]");
  //    } else {
  //      lcd.print(key);
  //      lcd.print("    ");
  //    }
  //  }
  //}
  // set second iteration interval
  if (debouncer2.fallingEdge()) {
    if(state == stIdle) {
      state = stSetKey;
      myEnc.write(loopTime2);
      lcd.setCursor(0,0);
      lcd.print("Press interval (ms):");
      lcd.blink();
    } else if(state == stSetKey) {
      state = stIdle;
      lcd.noBlink();
      lcd.setCursor(0,0);
      lcd.print("HLD Cheater         ");
      lcd.setCursor(0,1);
      lcd.print("                    ");
      lcd.setCursor(5,3);
      lcd.print("     ");
      lcd.setCursor(5,3);
      lcd.print(loopTime2);
    }
  }
  // set number of keystrokes to send
  if (debouncer3.fallingEdge()) {
    if(state == stIdle) {
      state = stSetIterations;
      myEnc.write(iterations);
      lcd.setCursor(0,0);
      lcd.print("Number of presses:  ");
      lcd.blink();
    } else if(state == stSetIterations) {
      state = stIdle;
      lcd.noBlink();
      lcd.setCursor(0,0);
      lcd.print("HLD Cheater         ");
      lcd.setCursor(0,1);
      lcd.print("                    ");
      lcd.setCursor(10,3);
      lcd.print("     ");
      lcd.setCursor(10,3);
      lcd.print(iterations);
    }
  }
  // set initial iteration interval (first 3 dashes require a slightly longer interval)
  if (debouncer4.fallingEdge()) {
    if(state == stIdle) {
      state = stSetLoopTime;
      myEnc.write(loopTime);
      lcd.setCursor(0,0);
      lcd.print("Press interval (ms):");
      lcd.blink();
    } else if(state == stSetLoopTime) {
      state = stIdle;
      lcd.noBlink();
      lcd.setCursor(0,0);
      lcd.print("HLD Cheater         ");
      lcd.setCursor(0,1);
      lcd.print("                    ");
      lcd.setCursor(15,3);
      lcd.print("     ");
      lcd.setCursor(15,3);
      lcd.print(loopTime);
    }
  }
  switch (state) {
    case 0: //idle
      break;
    case 1: //running
      keyPress();
      break;
    case 2: //setKey
      //setKey();
      setLoopTime2();
      break;
    case 3: //setIterations
      setIterations();
      break;
    case 4: //setLoopTime
      setLoopTime();
      break;
  }
  switchDigit();
}

// keep pressing key for [pressTime] milliseconds. Sending zero length keystrokes didn't work
void keyPress() {
  if (isKeyPressed == 1 && pressTimer >= pressTime) {
    Keyboard.release(KEY_SPACE);
    isKeyPressed = 0;
  }
  if (iterationCounter <= 3) {
    if (keyTimer >= loopTime) {
      keyTimer = 0;
      iterationCounter++;
      if (iterationCounter > iterations) {
        iterationCounter = 0;
        state = stIdle;
        lcd.setCursor(0,3);
        lcd.print("strt");
      } else {
        //Keyboard.print(key);
        Keyboard.press(KEY_SPACE);
        isKeyPressed = 1;
        pressTimer = 0;
      }
    }
  } else {
    if (keyTimer >= loopTime2) {
      keyTimer = 0;
      iterationCounter++;
      if (iterationCounter > iterations) {
        iterationCounter = 0;
        state = stIdle;
        lcd.setCursor(0,3);
        lcd.print("strt");
      } else {
        //Keyboard.print(key);
        Keyboard.press(KEY_SPACE);
        isKeyPressed = 1;
        pressTimer = 0;
      }
    }
  }
}

// set key by translating number to char using ASCII code
//void setKey() {
//  long newPosition = myEnc.read();
//  if (newPosition != oldPosition) {
//    oldPosition = newPosition;
//    lcd.setCursor(0,1);
//    lcd.print("                    ");
//    lcd.setCursor(0,1);
//    lcd.print(char(newPosition));
//    key = char(newPosition);
//  }
//}

void setIterations() {
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
    lcd.setCursor(0,1);
    lcd.print("                    ");
    lcd.setCursor(0,1);
    lcd.print(newPosition);
    iterations = newPosition;
  }
}

void setLoopTime() {
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
    lcd.setCursor(0,1);
    lcd.print("                    ");
    lcd.setCursor(0,1);
    lcd.print(newPosition);
    loopTime = newPosition;
  }
}

void setLoopTime2() {
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
    lcd.setCursor(0,1);
    lcd.print("                    ");
    lcd.setCursor(0,1);
    lcd.print(newPosition);
    loopTime2 = newPosition;
  }
}

// Seven Segment display is multiplexed. Loop runs fast enough that switching digit each loop is inperceptable.
void switchDigit() {
  switch (digitEnumerator) {
    case 1:
      digitalWrite(ones, HIGH);
      data = dataArray[iterationCounter / 1000];
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, data);
      digitalWrite(latchPin, 1);
      digitalWrite(thousands, LOW);
      digitEnumerator++;
      break;
    case 2:
      digitalWrite(thousands, HIGH);
      data = dataArray[(iterationCounter / 100) % 10];
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, data);
      digitalWrite(latchPin, 1);
      digitalWrite(hundreds, LOW);
      digitEnumerator++;
      break;
    case 3:
      digitalWrite(hundreds, HIGH);
      data = dataArray[(iterationCounter / 10) % 10];
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, data);
      digitalWrite(latchPin, 1);
      digitalWrite(tens, LOW);
      digitEnumerator++;
      break;
    case 4:
      digitalWrite(tens, HIGH);
      data = dataArray[iterationCounter % 10];
      digitalWrite(latchPin, 0);
      shiftOut(dataPin, clockPin, data);
      digitalWrite(latchPin, 1);
      digitalWrite(ones, LOW);
      digitEnumerator = 1;
      break;
  }
}

// pushing values through a SIPO shift register to the seven segments (eight, including dot)
void shiftOut(int myDataPin, int myClockPin, byte myDataOut) {
  // This shifts 8 bits out MSB first,
  //on the rising edge of the clock,
  //clock idles low

  //internal function setup
  int i=0;
  int pinState;
  

  //clear everything out just in case to
  //prepare shift register for bit shifting
  digitalWrite(myDataPin, 0);
  digitalWrite(myClockPin, 0);

  //for each bit in the byte myDataOut�
  //NOTICE THAT WE ARE COUNTING DOWN in our for loop
  //This means that %00000001 or "1" will go through such
  //that it will be pin Q0 that lights.
  for (i=7; i>=0; i--)  {
    digitalWrite(myClockPin, 0);

    //if the value passed to myDataOut and a bitmask result
    // true then... so if we are at i=6 and our value is
    // %11010100 it would the code compares it to %01000000
    // and proceeds to set pinState to 1.
    if ( myDataOut & (1<<i) ) {
      pinState= 1;
    }
    else { 
      pinState= 0;
    }

    //Sets the pin to HIGH or LOW depending on pinState
    digitalWrite(myDataPin, pinState);
    //register shifts bits on upstroke of clock pin  
    digitalWrite(myClockPin, 1);
    //zero the data pin after shift to prevent bleed through
    digitalWrite(myDataPin, 0);
  }

  //stop shifting
  digitalWrite(myClockPin, 0);
}
